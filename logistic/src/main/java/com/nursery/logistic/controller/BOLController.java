package com.nursery.logistic.controller;

import com.nursery.logistic.model.dto.BOLDTO;
import com.nursery.logistic.model.entity.BOL;
import com.nursery.logistic.model.response.ResponseModel;
import com.nursery.logistic.service.IBOLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bol")
public class BOLController {

    @Autowired
    private IBOLService orderService;

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @GetMapping("/{id}")
    public ResponseModel one(@PathVariable Long id) {
        try {
            BOL odr = orderService.findById(id);
            BOLDTO lddto = new BOLDTO();
            lddto.setBol(odr);
//    		lddto.setUser(usrClient.getUserByID(odr.getUserId()));
//    		lddto.setNote(usrClient.getError());
            return new ResponseModel(true, lddto);
        } catch (Exception e) {
            return new ResponseModel(false, null, e.getMessage());
        }

    }

    @PostMapping("publicMessage")
    public ResponseModel publicMessage(@Param(value = "") String msg) {
        try {
            kafkaTemplate.send("topicTest", msg);

            return new ResponseModel<>(true, null, null);
        } catch(Exception e) {
            return new ResponseModel(false, null, e.toString());
        }
    }
}