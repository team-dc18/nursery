package com.nursery.logistic.service;

import com.nursery.logistic.model.entity.BOL;

public interface IBOLService {
	BOL findById(Long id);
}
