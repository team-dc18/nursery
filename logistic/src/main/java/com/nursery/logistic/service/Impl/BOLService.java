package com.nursery.logistic.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nursery.logistic.model.entity.BOL;
import com.nursery.logistic.repository.BOLRepository;
import com.nursery.logistic.service.IBOLService;

@Service
public class BOLService implements IBOLService {

	@Autowired
	BOLRepository bolRepo;

	@Override
	public BOL findById(Long id) {
		return bolRepo.findById(id).get();
	}


}
