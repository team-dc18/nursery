package com.nursery.logistic.model.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ResponseModel<T> implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private T data;

    private Boolean success = false;
    private String errorMessage = "";

    public ResponseModel(Boolean success, T oData) {
        this.data = oData;
        this.success = success;
        this.errorMessage = "";
    }

    public ResponseModel(Boolean success, T oData, String errorMessage) {
        this.data = oData;
        this.success = success;
        this.errorMessage = errorMessage;
    }

    public ResponseModel() {
        this.data = null;
    }
}
