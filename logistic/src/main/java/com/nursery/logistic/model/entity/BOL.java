package com.nursery.logistic.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;

@Entity(name = "bol")
@Getter
@Setter
public class BOL implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user-sequence-generator")
    @SequenceGenerator(name = "user-sequence-generator", sequenceName = "user_sequence", initialValue = 1, allocationSize = 1)
    private Long id;

    @Column(name = "bol_no")
    private String bolNo;

    @Column(name = "user_id")
    private Long userId;
}