#!/bin/sh

echo "********************************************************"
echo "Starting Logistic Server with Configuration Service :  $CONFIGSERVER_URI";
echo "********************************************************"
java -Djava.security.egd=file:/dev/./urandom -Dserver.port=$SERVER_PORT -Deureka.client.serviceUrl.defaultZone=$EUREKASERVER_URI -Dspring.cloud.config.uri=$CONFIGSERVER_URI -Dspring.profiles.active=$PROFILE -Dspring.devtools.restart.enabled=true -jar /usr/local/logistic/@project.build.finalName@.jar