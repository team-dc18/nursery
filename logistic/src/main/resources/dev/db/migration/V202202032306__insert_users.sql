create table user
(
    id        bigint not null
        constraint user_pkey
            primary key,
    address   varchar(255),
    email     varchar(255),
    firstname varchar(255),
    lastname  varchar(255),
    password  varchar(255),
    role      varchar(255)
);

INSERT INTO "user"(id, address, email, firstname, lastname, password, role) VALUES (1, 'Quy Nhonw', 'tdat1@tma.com.vn', 'dat', 'tran', '12345678x@X', 'ADMIN');