package com.nursery.common.model.response.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import com.nursery.common.model.response.AbstractResponse;

@Getter
@Setter
@AllArgsConstructor
public class AbstractErrorResponse extends AbstractResponse {
    protected int code;
    protected String message;
}
