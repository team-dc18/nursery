package com.nursery.common.model.response.success;

import lombok.Getter;
import lombok.Setter;
import com.nursery.common.model.response.AbstractResponse;

@Getter
@Setter
public abstract class AbstractResultResponse<T> extends AbstractResponse {
    protected int code;
    protected String message;
    protected T data;

    public AbstractResultResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
