package com.nursery.common.constant;

public enum ERole {
    ROLE_SUPER_ADMIN,
    ROLE_ADMIN,
    ROLE_EMPLOYEE,
}