package com.nursery.catalog.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("catalogs")
@AllArgsConstructor
public class UserController {

    @GetMapping
    public String getMessage() {
        return "Demo Catalog controller";
    }
}
