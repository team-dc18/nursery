package com.nursery.catalog.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity(name = "GroupTypes")
@Setter
@Getter
public class GroupType extends AbstractEntity implements Serializable {
    private String name;
    private int level;
}
