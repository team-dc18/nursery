package com.nursery.catalog.model.projection;

public interface WarehouseListView {
    String getName();
    String getAddress();
    String getRepoNo();
}
