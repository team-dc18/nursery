package com.nursery.catalog.model.entity;

import lombok.Getter;
import lombok.Setter;
import com.nursery.common.constant.ActiveStatus;
import com.nursery.common.constant.DeleteStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity(name = "Products")
@Setter
@Getter
public class Product extends AbstractEntity implements Serializable {
    private String name;
    private String company;
    private String producer;
    private String accessoryId;
    private String sku;

    @Column(columnDefinition = "text")
    private String description;

    @Column(columnDefinition = "SMALLINT default 1", nullable = false, insertable = false)
    private ActiveStatus status; // 1: active, 0: inactive

    @Column(columnDefinition = "SMALLINT default 0", nullable = false, insertable = false)
    private DeleteStatus isDeleted;

}
