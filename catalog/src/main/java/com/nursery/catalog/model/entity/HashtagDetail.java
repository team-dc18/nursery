package com.nursery.catalog.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity(name = "HashtagDetails")
@Setter
@Getter
public class HashtagDetail implements Serializable {
    @EmbeddedId
    private HashtagCompositeKey hashtagCompositeKey;
}
