package com.nursery.catalog.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity(name = "CatalogDetails")
@Setter
@Getter
public class CatalogDetail implements Serializable {
    @EmbeddedId
    private CatalogCompositeKey catalogCompositeKey;
}
