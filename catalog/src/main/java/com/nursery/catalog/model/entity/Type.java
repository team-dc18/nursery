package com.nursery.catalog.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity(name = "Types")
@Setter
@Getter
public class Type extends AbstractEntity implements Serializable {
    private String name;

    @ManyToOne
    private GroupType groupType;
}
