package com.nursery.catalog.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
public class HashtagCompositeKey implements Serializable {
    @Column(name = "hashtag_id", nullable = false)
    private long hashtagId;

    @Column(name = "product_id", nullable = false)
    private long productId;

    /** getters and setters **/
}
