package com.nursery.catalog.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity(name = "ProductDetails")
@Setter
@Getter
public class ProductDetail extends AbstractEntity implements Serializable {
    @ManyToOne
    private Product product;

    @ManyToOne
    private Type level1;

    @ManyToOne
    private Type level2;
}
