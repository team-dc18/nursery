package com.nursery.catalog.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity(name = "Hashtags")
@Setter
@Getter
public class Hashtag extends AbstractEntity implements Serializable {
    private String name;
}
