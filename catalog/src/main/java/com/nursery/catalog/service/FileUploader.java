package com.nursery.catalog.service;

import io.minio.*;
import io.minio.errors.MinioException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class FileUploader {
  public static void main(String[] args)
      throws IOException, NoSuchAlgorithmException, InvalidKeyException {
    try {
      // Create a minioClient with the MinIO server playground, its access key and secret key.
      MinioClient minioClient =
          MinioClient.builder()
              .endpoint("http://localhost:9000")
              .credentials("minio", "m1n1os@rv@r")
              .build();

      // Make 'minio-test' bucket if not exist.
      boolean found =
          minioClient.bucketExists(BucketExistsArgs.builder().bucket("minio-test-1").build());
      if (!found) {
        // Make a new bucket called 'minio-test'.
        minioClient.makeBucket(MakeBucketArgs.builder().bucket("minio-test-1").build());
      } else {
        System.out.println("Bucket 'minio-test-1' already exists.");
      }

      // Upload '/home/user/Photos/asiaphotos.zip' as object name 'asiaphotos-2015.zip' to bucket
      // 'minio-test'.
      ObjectWriteResponse res = minioClient.uploadObject(
          UploadObjectArgs.builder()
              .bucket("minio-test-1")
              .object("Session_1.pdf")
              .filename("E:\\Videos\\spring-microservice-training\\related\\Session_1.pdf")
              .build());
      System.out.println(res.region());
      System.out.println(
          "'E:\\Videos\\spring-microservice-training\\related\\Session_1.pdf' is successfully uploaded as "
              + "object 'Session_1.pdf' to bucket 'minio-test'.");
    } catch (MinioException e) {
      System.out.println("Error occurred: " + e);
      System.out.println("HTTP trace: " + e.httpTrace());
    }
  }
}
