package com.nursery.inventory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.nursery.inventory.model.entity.Catalog;

import java.util.Optional;

public interface CatalogRepository extends JpaRepository<Catalog, Long> {
    Optional<Catalog> findByProductId(long productId);
}
