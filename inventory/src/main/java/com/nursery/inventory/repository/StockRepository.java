package com.nursery.inventory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.nursery.inventory.model.entity.Stock;

import java.util.Optional;

public interface StockRepository extends JpaRepository<Stock, Long> {
    Optional<Stock> findDistinctByCompositeKey_CatalogIdAndCompositeKey_WarehouseIdAndTypeId(long catalogId, long warehouseId, long typeId);
}
