package com.nursery.inventory.service.impl;

import com.nursery.inventory.model.request.CreateWarehouseRequest;
import com.nursery.inventory.model.request.UpdateWarehouseRequest;
import com.nursery.inventory.repository.WarehouseRepository;
import com.nursery.inventory.service.WarehouseService;
import com.nursery.inventory.validator.WarehouseValidator;
import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.nursery.inventory.model.entity.Warehouse;
import com.nursery.common.constant.DeleteStatus;
import com.nursery.common.exception.ServiceException;
import com.nursery.common.exception.custom.NotFoundException;
import com.nursery.inventory.model.projection.WarehouseListView;

import javax.persistence.EntityExistsException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class WarehouseServiceImpl implements WarehouseService {
    private final WarehouseRepository warehouseRepository;
    private final MessageSource messages;

    @Override
    public Warehouse createWarehouse(CreateWarehouseRequest createWarehouseRequest) {
        // Preprocessing input data
        createWarehouseRequest.setName(createWarehouseRequest.getName().trim());
        createWarehouseRequest.setRepoNo(createWarehouseRequest.getRepoNo().trim());
        createWarehouseRequest.setLongitude(createWarehouseRequest.getLongitude().trim());
        createWarehouseRequest.setLatitude(createWarehouseRequest.getLatitude().trim());
        createWarehouseRequest.setAddress(createWarehouseRequest.getAddress().trim());

        Optional<Warehouse> localWarehouseOptional = warehouseRepository.findWarehouseByNameAndIsDeleted(createWarehouseRequest.getName(), DeleteStatus.NOT_DELETED);

        if (localWarehouseOptional.isPresent()) {
            throw new ServiceException(String.format(messages.getMessage("warehouse.create.error.name-existed", null, null), createWarehouseRequest.getName()));
        }

        Warehouse createdWarehouse = new Warehouse();
        createdWarehouse.setName(createWarehouseRequest.getName());
        createdWarehouse.setRepoNo(createWarehouseRequest.getRepoNo());
        createdWarehouse.setLongitude(createWarehouseRequest.getLongitude());
        createdWarehouse.setLatitude(createWarehouseRequest.getLatitude());
        createdWarehouse.setAddress(createWarehouseRequest.getAddress());

        return warehouseRepository.save(createdWarehouse);
    }

    @Override
    public Warehouse updateWarehouse(long id, UpdateWarehouseRequest updateWarehouseRequest) {
        Optional<Warehouse> localWarehouseOptional = warehouseRepository.findWarehouseByIdAndIsDeleted(id, DeleteStatus.NOT_DELETED);
        Warehouse localWarehouse = localWarehouseOptional.get();
        if (localWarehouseOptional.isPresent()) {

            localWarehouse.setName(updateWarehouseRequest.getName() != null ? updateWarehouseRequest.getName().trim() : localWarehouse.getName());
            localWarehouse.setRepoNo(updateWarehouseRequest.getRepoNo() != null ? updateWarehouseRequest.getRepoNo().trim() : localWarehouse.getRepoNo());
            localWarehouse.setLongitude(updateWarehouseRequest.getLongitude() != null ? updateWarehouseRequest.getLongitude().trim() : localWarehouse.getLongitude());
            localWarehouse.setLatitude(updateWarehouseRequest.getLatitude() != null ? updateWarehouseRequest.getLatitude().trim() : localWarehouse.getLatitude());
            localWarehouse.setAddress(updateWarehouseRequest.getAddress() != null ? updateWarehouseRequest.getAddress().trim() : localWarehouse.getAddress());

            localWarehouse = warehouseRepository.save(localWarehouse);
        } else {
            throw new NotFoundException(String.format(messages.getMessage("warehouse.get.error.not-found", null, null), updateWarehouseRequest.getName()));
        }
        return localWarehouse;
    }

    @Override
    public Warehouse findWarehouseById(long id) {
        Optional<Warehouse> localWarehouseOptional = warehouseRepository.findWarehouseByIdAndIsDeleted(id, DeleteStatus.NOT_DELETED);
        if(localWarehouseOptional.isPresent()) {
            return localWarehouseOptional.get();
        } else {
            throw new NotFoundException(String.format(messages.getMessage("warehouse.get.error.not-found", null, null), id));
        }
    }

    @Override
    public Page<WarehouseListView> listAllExistedWarehouses(Pageable pageable) {
        return warehouseRepository.findByIsDeleted(DeleteStatus.NOT_DELETED, pageable);
    }

    @Override
    public Page<WarehouseListView> listAllExistedWarehousesWithQuery(String query, Pageable pageable) {
        query = query.trim();
        return warehouseRepository.findByNameLikeOrRepoNoLikeOrAddressLike(query, query, query, DeleteStatus.NOT_DELETED, pageable);
    }

    @Override
    public int deletedWarehouseById(long id) {
        Optional<Warehouse> localWarehouseOptional = warehouseRepository.findWarehouseByIdAndIsDeleted(id, DeleteStatus.NOT_DELETED);
        if(localWarehouseOptional.isPresent()) {
            return warehouseRepository.setIsDeletedFor(DeleteStatus.DELETED, id);
        }
        return -1;
    }

    @Override
    public Warehouse create(WarehouseValidator validatedData) {
        Warehouse warehouse = new Warehouse();

        if (this.warehouseRepository.exists(validatedData.getName())) {
            throw new EntityExistsException("Name is already in use");
        }

        if (this.warehouseRepository.exists(validatedData.getCode())) {
            throw new EntityExistsException("Code is already existed");
        }
        createOrUpdateWareHouse(validatedData, warehouse);
        return this.warehouseRepository.save(warehouse);
    }

    private void createOrUpdateWareHouse(WarehouseValidator validatedData, Warehouse instance){
        instance.setName(validatedData.getName());
        instance.setRepoNo(validatedData.getCode());
        instance.setLatitude(validatedData.getLatitude());
        instance.setLongitude(validatedData.getLongitude());
        instance.setAddress(validatedData.getAddress());
        instance.setStatus(validatedData.getStatus());
    }
}
