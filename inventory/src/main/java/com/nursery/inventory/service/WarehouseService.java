package com.nursery.inventory.service;

import com.nursery.inventory.model.request.CreateWarehouseRequest;
import com.nursery.inventory.model.request.UpdateWarehouseRequest;
import com.nursery.inventory.validator.WarehouseValidator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import com.nursery.inventory.model.entity.Warehouse;
import com.nursery.inventory.model.projection.WarehouseListView;

public interface WarehouseService {

    /**
     * Create a warehouse if not existed
     *
     * @param createWarehouseRequest: some info for creating warehouse
     * @return Warehouse
     */
    @Transactional
    Warehouse createWarehouse(CreateWarehouseRequest createWarehouseRequest);

    /**
     * Update a warehouse
     *
     * @param updateWarehouseRequest: some info for updating warehouse
     * @return Warehouse
     */
    @Transactional
    Warehouse updateWarehouse(long id, UpdateWarehouseRequest updateWarehouseRequest);

    /**
     * Find a warehouse with isDeleted = 0 (not deleted yet) by ID
     * @return WarehouseListView
     */
    @Transactional(readOnly = true)
    Warehouse findWarehouseById(long id);

    /**
     * List all existed warehouses with isDeleted = 0 (not deleted yet) and without query
     * @param pageable: some info to pagination
     * @return WarehouseListView
     */
    @Transactional(readOnly = true)
    Page<WarehouseListView> listAllExistedWarehouses(Pageable pageable);

    /**
     * List all existed warehouses with isDeleted = 0 (not deleted yet) and with query
     * @param query:    input to search warehouse (name, repoNo, address)
     * @param pageable: some info to pagination
     * @return WarehouseListView
     */
    @Transactional(readOnly = true)
    Page<WarehouseListView> listAllExistedWarehousesWithQuery(String query, Pageable pageable);

    /**
     * Delete a warehouse by ID
     * => Set isDeleted = 1
     * @return int
     */
    @Transactional
    int deletedWarehouseById(long id);

    Warehouse create(WarehouseValidator validatedData);

}
