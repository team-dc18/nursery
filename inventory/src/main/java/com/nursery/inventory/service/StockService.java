package com.nursery.inventory.service;

import com.nursery.inventory.model.request.UpdateAvailableProductRequest;
import org.springframework.transaction.annotation.Transactional;

public interface StockService {

    /**
     * Update a warehouse
     *
     * @param updateAvailableProductRequest: some info for updating stock for a product
     */
    @Transactional
    void updateAvailable(UpdateAvailableProductRequest updateAvailableProductRequest);
}
