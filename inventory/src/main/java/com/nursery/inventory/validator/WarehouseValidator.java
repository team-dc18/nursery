package com.nursery.inventory.validator;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.nursery.common.constant.ActiveStatus;

import javax.validation.constraints.*;

@Data
@Slf4j
public class WarehouseValidator {
    @NotBlank(message = "Field name is required")
    @Size(max = 50, message = "Field name cannot be over 50 characters")
    private String name;

    @NotBlank(message = "Field name is required")
    @Size(max = 50, message = "Field name cannot be over 50 characters")
    private String code;

    @NotNull(message = "Field longitude is required")
    private String longitude;

    @NotNull(message = "Field latitude is required")
    private String latitude;

    @NotNull(message = "Field latitude is required")
    private String address;

    private ActiveStatus status;
}
