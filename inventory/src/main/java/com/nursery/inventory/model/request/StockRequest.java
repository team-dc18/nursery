package com.nursery.inventory.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;

@Getter
@Setter
@AllArgsConstructor
public class StockRequest {
    private Long repoId;
    private Long typeId;

    @Min(0)
    private Long available;

    @Min(0)
    private Long price;

    private String sku;
}
