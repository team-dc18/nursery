package com.nursery.inventory.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class UpdateAvailableProductRequest {
    @NotNull
    private Long productId;

    private List<StockRequest> stocks;
}
