package com.nursery.inventory.model.projection;

public interface SubjectListView {
    long getId();
    String getCourseName();
}
