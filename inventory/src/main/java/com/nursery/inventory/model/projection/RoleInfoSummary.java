package com.nursery.inventory.model.projection;

public interface RoleInfoSummary {
    String getName();
}