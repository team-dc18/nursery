package com.nursery.inventory.model.entity;

import lombok.Getter;
import lombok.Setter;
import com.nursery.common.constant.ActiveStatus;
import com.nursery.common.constant.DeleteStatus;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "Warehouses")
@Getter
@Setter
public class Warehouse extends AbstractEntity implements Serializable {
    private String name;
    private String repoNo;
    private String longitude;
    private String latitude;

    @Column(columnDefinition = "text")
    private String address;

    @Column(columnDefinition = "SMALLINT default 1", nullable = false, insertable = false)
    private ActiveStatus status; // 1: active, 0: inactive

    @Column(columnDefinition = "SMALLINT default 0", nullable = false, insertable = false)
    private DeleteStatus isDeleted;
}
