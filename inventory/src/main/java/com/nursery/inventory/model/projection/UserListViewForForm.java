package com.nursery.inventory.model.projection;

public interface UserListViewForForm {
    String getFullName();
    String getUsername();
    String getEmail();
}
