package com.nursery.inventory.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
public class CompositeKey implements Serializable {
    @Column(name = "catalog_id", nullable = false)
    private long catalogId;

    @Column(name = "warehouse_id", nullable = false)
    private long warehouseId;

    /** getters and setters **/
}