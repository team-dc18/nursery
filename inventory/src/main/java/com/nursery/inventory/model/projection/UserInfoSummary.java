package com.nursery.inventory.model.projection;

import java.util.Collection;

public interface UserInfoSummary {
    String getUsername();
    String getFullName();
    Collection<RoleInfoSummary> getRoles();
}
