package com.nursery.inventory.model.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class JwtTokenRequest {

	@Size(min = 5, max = 128)
	private String username;

	@Size(min = 8, max = 128)
	private String password;
}