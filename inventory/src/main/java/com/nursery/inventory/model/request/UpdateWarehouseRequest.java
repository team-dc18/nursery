package com.nursery.inventory.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
public class UpdateWarehouseRequest {
    @Size(min = 3, max = 50)
    private String name;

    @Size(min = 3, max = 50)
    private String repoNo;

    @Size(min = 3, max = 50)
    private String longitude;

    @Size(min = 3, max = 50)
    private String latitude;

    @Size(min = 3, max = 255)
    private String address;
}
