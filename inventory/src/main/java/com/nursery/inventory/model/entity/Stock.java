package com.nursery.inventory.model.entity;

import lombok.Getter;
import lombok.Setter;
import com.nursery.common.constant.ActiveStatus;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity(name = "Stocks")
@Setter
@Getter
public class Stock implements Serializable {
    @EmbeddedId
    private CompositeKey compositeKey;

    private long typeId;
    private long available;
    private long price;

    private String sku;

    @Column(columnDefinition = "SMALLINT default 1", nullable = false, insertable = false)
    private ActiveStatus status; // 1: active, 0: inactive
}