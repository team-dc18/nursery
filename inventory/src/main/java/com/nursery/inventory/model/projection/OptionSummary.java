package com.nursery.inventory.model.projection;

public interface OptionSummary {
    long getId();
    String getOptionContent();
}
