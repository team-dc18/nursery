package com.nursery.inventory.model.projection;

public interface WarehouseListView {
    String getName();
    String getRepoNo();
    String getAddress();
}
