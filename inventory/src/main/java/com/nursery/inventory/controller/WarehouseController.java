package com.nursery.inventory.controller;

import com.nursery.common.exception.custom.CustomMethodArgumentNotValidException;
import com.nursery.common.model.response.AbstractResponse;
import com.nursery.common.model.response.error.BaseErrorResponse;
import com.nursery.common.model.response.success.BaseResultResponse;
import com.nursery.common.model.response.success.PageInfo;
import com.nursery.common.model.response.success.PagingResultResponse;
import com.nursery.inventory.model.request.CreateWarehouseRequest;
import com.nursery.inventory.model.request.UpdateAvailableProductRequest;
import com.nursery.inventory.model.request.UpdateWarehouseRequest;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.nursery.inventory.model.entity.Warehouse;
import com.nursery.inventory.model.projection.WarehouseListView;
import com.nursery.inventory.service.StockService;
import com.nursery.inventory.service.WarehouseService;

import javax.validation.Valid;

@RestController
@RequestMapping("/repositories")
@AllArgsConstructor
public class WarehouseController {
    private static final Logger logger = LoggerFactory.getLogger(WarehouseController.class);
    private final WarehouseService warehouseService;
    private final StockService stockService;

    @PostMapping
    public AbstractResponse createWarehouse(@Valid @RequestBody CreateWarehouseRequest createWarehouseRequest,
                                            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new CustomMethodArgumentNotValidException(bindingResult);
        } else {
            Warehouse createdWarehouse = warehouseService.createWarehouse(createWarehouseRequest);
            return new BaseResultResponse<>(HttpStatus.OK.value(), createdWarehouse);
        }
    }

    @PutMapping("{id}")
    public AbstractResponse updateWarehouse(@PathVariable Long id,
                                            @Valid @RequestBody UpdateWarehouseRequest updateWarehouseRequest,
                                            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new CustomMethodArgumentNotValidException(bindingResult);
        } else {
            Warehouse updatedWarehouse = warehouseService.updateWarehouse(id, updateWarehouseRequest);
            return new BaseResultResponse<>(HttpStatus.OK.value(), updatedWarehouse);
        }
    }

    @GetMapping("{id}")
    public AbstractResponse getWarehouseById(@PathVariable Long id) {
        logger.info("Get warehouse by ID = {}", id);
        Warehouse result = warehouseService.findWarehouseById(id);
        return new BaseResultResponse<>(HttpStatus.OK.value(), result);
    }

    @GetMapping
    public AbstractResponse listAllExistedWarehouses(@RequestParam(name = "page", required = false, defaultValue = "0") int page,
                                                     @RequestParam(name = "size", required = false, defaultValue = "25") int size,
                                                     @RequestParam(name = "query", required = false, defaultValue = "") String query,
                                                     @RequestParam(name = "sort", required = false, defaultValue = "name") String sort) {
        Page<WarehouseListView> resultPage;
        Pageable pageable = PageRequest.of(page, size, Sort.by(sort).ascending());
        if (query == null) {
            resultPage = warehouseService.listAllExistedWarehouses(pageable);
        } else {
            resultPage = warehouseService.listAllExistedWarehousesWithQuery(query, pageable);
        }

        return new PagingResultResponse<>(
                HttpStatus.OK.value(),
                resultPage.getContent(),
                new PageInfo(
                        page,
                        size,
                        (int) resultPage.getTotalElements(),
                        resultPage.getTotalPages()
                )
        );
    }

    @DeleteMapping("{id}")
    public AbstractResponse deleteWarehouseById(@PathVariable Long id) {
        int recordAfterDeleted = warehouseService.deletedWarehouseById(id);
        if (recordAfterDeleted == 1) {
            return new BaseResultResponse<>(
                    HttpStatus.OK.value(),
                    "Warehouse has ID = " + id + " was be deleted.");
        }

        logger.error("Warehouse with ID = {} not found", id);
        return new BaseErrorResponse(
                HttpStatus.BAD_REQUEST.value(),
                "Warehouse has ID = " + id + " was not found for deleting. Nothing will be done!");
    }

    @PutMapping("product-stock")
    public AbstractResponse updateAvailableProduct(@Valid @RequestBody UpdateAvailableProductRequest updateAvailableProductRequest,
                                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new CustomMethodArgumentNotValidException(bindingResult);
        } else {
            stockService.updateAvailable(updateAvailableProductRequest);
            return new BaseErrorResponse(HttpStatus.OK.value(), "Update stock was successfully");
        }
    }
}
