<h3 style="text-align: center">
    BACKEND: Microservice E-commerce practice
</h3>

---

APIs Documentation
------------
- Tool: Swagger
- Link document: [http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html)

Code Conventions By Oracle
------------
- Link document: [https://www.oracle.com/java/technologies/javase/codeconventions-contents.html](https://www.oracle.com/java/technologies/javase/codeconventions-contents.html)

Installation
------------

Pre-conditions:
- JDK: [Oracle JDK version 8](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)
- IDE: IntelliJ IDEA Ultimate(recommended) or eclipse,...
- Database: can use docker or install directly [PostgreSQL](https://www.postgresql.org/download/) (recommended version 12.x)
- Libraries: [lombok](https://projectlombok.org/)
- Apache Kafka: [Kafka](https://kafka.apache.org/)
- Redis: [Redis](https://redis.io/download)
- Apache Zookeeper: [Zookeeper](https://zookeeper.apache.org/releases.html)

### Clone repo

``` bash
git clone https://gitlab.com/team-dc18/nursery.git
```

### Go to folder

``` bash
cd nursery/
```

<p align="center">
    <img src="/docker/Screenshot%202022-02-17111149.png" alt="Illustration for build project">
</p>

### Pull images and run as containers for project(database, cache, message queue,...)
``` bash
docker-compose -f docker/docker-compose.dev.yml up -d
```

### Run pre services
```text
1. configserver
2. eurekaserver
3. gateway
4. ...
```